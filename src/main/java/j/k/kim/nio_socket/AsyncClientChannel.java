package j.k.kim.nio_socket;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.charset.Charset;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class AsyncClientChannel {

	private int threadNumber;
	private AsynchronousSocketChannel clientChannel;
	private Future<Void> future;
	private Charset myCharset;
	
	public AsyncClientChannel(int threadNumber) throws IOException, InterruptedException, ExecutionException {
		this.threadNumber = threadNumber;
		myCharset = Charset.forName("UTF-8");
		clientChannel = AsynchronousSocketChannel.open();
		InetSocketAddress host = new InetSocketAddress("localhost", 12345);
		future = clientChannel.connect(host);
		future.get();
	}
	
	public void doRun() throws InterruptedException, ExecutionException {
		String sendMessage = "client send[" + threadNumber + "]";
		ByteBuffer writeBuffer = ByteBuffer.wrap(sendMessage.getBytes(myCharset));
		Future<Integer> writeResult = clientChannel.write(writeBuffer);
		int writeSize = writeResult.get();

		System.out.println("Send[" + sendMessage + "]");

		ByteBuffer readBuffer = ByteBuffer.allocate(1024);
		Future<Integer> readResult = clientChannel.read(readBuffer);

		int readSize = readResult.get();

		byte[] readArr = new byte[readSize];
		readBuffer.flip();
		readBuffer.get(readArr);
		readBuffer.clear();
		String receiveMessage = new String(readArr, myCharset);
		
		System.out.println("RECV[" + receiveMessage + "]");
	}
	
	public static void main(String[] args) throws InterruptedException {
		ExecutorService service = Executors.newFixedThreadPool(10);
		IntStream.range(0, 10).forEach(i -> {
			service.submit(() -> {
				try {
					AsyncClientChannel myClient = new AsyncClientChannel(i);
					myClient.doRun();
				} catch (IOException | InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}
				
			});
		});
		service.awaitTermination(5, TimeUnit.SECONDS);
		service.shutdown();
	}
}
