package j.k.kim.spring_integration_socket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.serializer.Deserializer;
import org.springframework.core.serializer.Serializer;

public class CustomSerializerDeserializer implements Serializer<SampleVO>, Deserializer<SampleVO> {

	private static final int ID_LENGTH = 3;
	private static final int NAME_LENGTH = 200;
	private static final int DATE_LENGTH = 19;
	
	Charset myCharset = Charset.forName("UTF-8");
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd/HH:mm:ss");

	
	@Override
	public void serialize(SampleVO object, OutputStream outputStream) throws IOException {
		System.out.println("Serialize[" + object + "]");
		ByteBuffer buffer = ByteBuffer.allocate(ID_LENGTH);
		byte[] idBytes =  fillArray(ID_LENGTH, String.valueOf(object.getId()).getBytes(myCharset));
		
		buffer = ByteBuffer.allocate(NAME_LENGTH);
		buffer.put(object.getName().getBytes(myCharset));
		byte[] nameBytes = buffer.array();

		buffer = ByteBuffer.allocate(DATE_LENGTH);
		
		String yyyyMMdd = sdf.format(object.getToDay());
		buffer.put(yyyyMMdd.getBytes(myCharset));
		byte[] dateBytes = buffer.array();

		ByteBuffer allBuffer = ByteBuffer.allocate(ID_LENGTH + NAME_LENGTH + DATE_LENGTH);
		allBuffer.put(idBytes);
		allBuffer.put(nameBytes);
		allBuffer.put(dateBytes);
		
		outputStream.write(allBuffer.array());

		outputStream.flush();
	}

	@Override
	public SampleVO deserialize(InputStream inputStream) throws IOException {
		String echoString = parseString(inputStream, "echo:".length());
		String idString = parseString(inputStream, ID_LENGTH);
		String nameString = parseString(inputStream, NAME_LENGTH);
		String dateString = parseString(inputStream, DATE_LENGTH);
		System.out.println("DESERIALIZE[" + echoString + "][" + idString + "][" + nameString + "][" + dateString + "]");
		SampleVO item = new SampleVO();
		item.setId(Integer.parseInt(idString.trim()));
		item.setName(nameString.trim());
		try {
			item.setToDay(sdf.parse(dateString.trim()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return item;
	}

	private String parseString(InputStream inputStream, int length) throws IOException {
		StringBuilder builder = new StringBuilder();

		int c;
		for (int i = 0; i < length; ++i) {
			c = inputStream.read();
			checkClosed(c);
			builder.append((char)c);
		}

		return builder.toString();
	}

	protected void checkClosed(int bite) throws IOException {
		if (bite < 0) {
			throw new IOException("Socket closed during message assembly");
		}
	}
	
	private byte[] fillArray(int destLength, byte[] origin) {
		ByteBuffer buffer = ByteBuffer.allocate(destLength);
		buffer.put(origin);
		return buffer.array();
	}
	
	private String pad(int desiredLength, int length) {
		return StringUtils.leftPad(Integer.toString(length), desiredLength, '0');
	}
	
}
