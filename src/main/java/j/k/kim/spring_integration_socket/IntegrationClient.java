package j.k.kim.spring_integration_socket;

import java.util.Date;

import org.springframework.context.support.GenericXmlApplicationContext;

public class IntegrationClient {


	public static void main(final String... args) {

		final GenericXmlApplicationContext context = setupContext();

		System.out.println("###############################################");
		System.out.println("Client Start");
		System.out.println("###############################################");

		ServiceInterface gateway = context.getBean(ServiceInterface.class);


		SampleVO item = new SampleVO();
		item.setId(1);
		item.setName("한글 hello");
		item.setToDay(new Date());
		
		SampleVO resp = gateway.send(item);
		System.out.println("resp1[" + resp + "]");
		
		item = new SampleVO();
		item.setId(2);
		item.setName("한글world");
		item.setToDay(new Date());
		resp = gateway.send(item);
		System.out.println("resp2[" + resp + "]");
		
//		String result = gateway.send("hello");
//		System.out.println("res[" + result + "]");
//		
//		result = gateway.send("world");
//		System.out.println("res[" + result + "]");

		System.out.println("Exiting application...bye.");
		System.exit(0);

	}

	public static GenericXmlApplicationContext setupContext() {

		final GenericXmlApplicationContext context = new GenericXmlApplicationContext();

		context.load("classpath:META-INF/spring/integration/clientConfig.xml");
		context.registerShutdownHook();
		context.refresh();

		return context;
	}
	
}
