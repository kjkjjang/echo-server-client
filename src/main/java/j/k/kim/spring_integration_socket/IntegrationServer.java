package j.k.kim.spring_integration_socket;

import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.integration.ip.tcp.connection.AbstractServerConnectionFactory;
import org.springframework.integration.ip.util.TestingUtilities;

public class IntegrationServer {

	public String echo(String input) {
		System.out.println("echo call[" + input + "]");
		if ("FAIL".equals(input)) {
			throw new RuntimeException("Failure Demonstration");
		}
		return "echo:" + input;
	}

	public static void main(String[] args) {
		final GenericXmlApplicationContext context = setupContext();

		final AbstractServerConnectionFactory crLfServer = context.getBean(AbstractServerConnectionFactory.class);

		System.out.println("###############################################");
		System.out.println("Server Start");
		System.out.println("###############################################");

		TestingUtilities.waitListening(crLfServer, 10000L);
		System.out.println("###############################################");
		System.out.println("Server running");
		System.out.println("###############################################");

		
	}
	
	public static GenericXmlApplicationContext setupContext() {

		final GenericXmlApplicationContext context = new GenericXmlApplicationContext();

		context.load("classpath:META-INF/spring/integration/serverConfig.xml");
		context.registerShutdownHook();
		context.refresh();

		return context;
	}
}
