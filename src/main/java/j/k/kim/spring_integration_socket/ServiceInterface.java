package j.k.kim.spring_integration_socket;


public interface ServiceInterface {

	public String send(String text);
	public SampleVO send(SampleVO item);

}